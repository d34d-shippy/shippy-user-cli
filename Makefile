build:
	docker build -t registry.gitlab.com/d34d-shippy/shippy-user-cli .
	docker push registry.gitlab.com/d34d-shippy/shippy-user-cli

run:
	docker run --net="host" \
	-e MICRO_REGISTRY=mdns \
	shippy-user-cli
